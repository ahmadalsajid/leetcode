# https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3240/
class Solution:
    # def sortedSquares(self, nums: List[int]) -> List[int]:
    def sortedSquares(self, nums):
        return sorted(list(map(lambda x: x ** 2, nums)))
