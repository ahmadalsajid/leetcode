# https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3259/

class Solution:
    # def replaceElements(self, arr: List[int]) -> List[int]:
    def replaceElements(self, arr):
        length = len(arr)
        if length == 1:
            return [-1]
        for i in range(0, length-1):
            arr[i] = max(arr[i+1:])
        arr[-1] = -1
        return arr
