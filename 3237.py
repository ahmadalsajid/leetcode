# https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3237/

class Solution:
    # def findNumbers(self, nums: List[int]) -> int:     # for leetcode submission
    def findNumbers(self, nums):
        return sum(list(map(lambda x: 0 if len(str(x)) % 2 else 1, nums)))
