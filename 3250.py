# https://leetcode.com/explore/learn/card/fun-with-arrays/527/searching-for-items-in-an-array/3250/

class Solution:
    # def checkIfExist(self, arr: List[int]) -> bool:
    def checkIfExist(self, arr):
        seen = {}
        for i in range(len(arr)):
            if 2 * arr[i] in seen or arr[i] % 2 == 0 and arr[i] // 2 in seen:
                return True
            seen[arr[i]] = i
        return False


