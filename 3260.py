# https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3260/

class Solution:
    # def sortArrayByParity(self, nums: List[int]) -> List[int]:
    def sortArrayByParity(self, nums):
        even = []
        odd = []

        for elem in nums:
            if elem % 2:
                odd.append(elem)
            else:
                even.append(elem)
        return even + odd