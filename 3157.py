# https://leetcode.com/explore/learn/card/fun-with-arrays/511/in-place-operations/3157/

class Solution:
    # def moveZeroes(self, nums: List[int]) -> None:
    def moveZeroes(self, nums):
        """
        Do not return anything, modify nums in-place instead.
        """
        count = 0
        try:
            while True:
                nums.remove(0)
                count = count + 1
        except ValueError:
            pass
        nums.extend([0 for i in range(count)])