# https://leetcode.com/explore/learn/card/fun-with-arrays/526/deleting-items-from-an-array/3248/

class Solution:
    # def removeDuplicates(self, nums: List[int]) -> int:
    def removeDuplicates(self, nums):
        i = 0
        ln = len(nums)
        for j in range(1, ln):
            if nums[i] != nums[j]:
                i += 1
                nums[i] = nums[j]
        i += 1
        return i
