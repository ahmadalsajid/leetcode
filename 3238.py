# https://leetcode.com/explore/learn/card/fun-with-arrays/521/introduction/3238/
import itertools


class Solution:
    # def findMaxConsecutiveOnes(self, nums: List[int]) -> int:     # for leetcode submission
    def findMaxConsecutiveOnes(self, nums):
        z = [(x[0], len(list(x[1]))) for x in itertools.groupby(nums)]
        z = sorted(z, key=lambda element: (element[0], element[1]))
        last_item = z[-1]
        if last_item[0]:
            return last_item[1]
        else:
            return 0
