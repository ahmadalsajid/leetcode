# https://leetcode.com/explore/learn/card/fun-with-arrays/525/inserting-items-into-an-array/3245/

class Solution:
    # def duplicateZeros(self, arr: List[int]) -> None:
    def duplicateZeros(self, arr):
        """
        Do not return anything, modify arr in-place instead.
        """
        length = len(arr)
        pos = 0
        while pos < length:
            if not arr[pos]:
                arr.insert(pos + 1, 0)
                pos = pos + 2
                arr.pop()
            else:
                pos = pos + 1
